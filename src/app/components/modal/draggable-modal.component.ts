import { Router } from '@angular/router';
import { Component, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

declare var $: any;

@Component({
  selector: 'draggable-modal',
  templateUrl: 'draggable-modal.component.html',
  styleUrls: ['./draggable-modal.scss'],
})

export class DraggableModalComponent implements OnInit {

  title: string = '';
  url: string = '';

  constructor(
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit() {

   }

  close() {
    this.activeModal.close();
  }

}
