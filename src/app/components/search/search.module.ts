import { ListLoadingComponent } from './../list-loading/list-loading.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DraggableModalModule } from '../modal/draggable-modal.module';


@NgModule({
  declarations: [
    SearchComponent,
    ListLoadingComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,DraggableModalModule
  ],
  exports: [
    SearchComponent
  ]
})
export class SearchModule { }
