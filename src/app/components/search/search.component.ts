import { DraggableModalComponent } from './../modal/draggable-modal.component';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { AppConfig } from './../../config/app.config';
import { HttpService } from './../../services/http.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, HostListener, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service';


const ModalConfig: NgbModalOptions = {
}
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [
    trigger('searchAnimation', [
      state('slideup', style({
        transform: 'translateY(-45%)'
      })),
      state('slidedown', style({
        transform: 'translateY(0%)'
      })),
      transition('* => slideup,void => *', [
        style({ opacity: 0 }), animate('500ms ease')
      ]),
    ]),

  ]
})
export class SearchComponent implements OnInit {
  searchbar: string;
  results: []
  loading: boolean = true;

  modalRef: any;
  modalOptions: NgbModalOptions = ModalConfig;

  constructor(private http: HttpService, private config: AppConfig, private modalService: NgbModal,
    private localStorageService: LocalStorageService) { }

  ngOnInit(): void {
    this.localStorageService.init();
  }

  searchAnimation(event) {
    console.log(event.keyCode)
    if ([13].indexOf(event.keyCode) !== -1 && event.target.value) {
      this.loading = true;
      this.searchbar = "slideup";
      if (this.localStorageService.youtubeResults) {
        this.results = this.localStorageService.youtubeResults;
        this.loading = false;
      } else {
        this.http.call({}, `/search?part=snippet&maxResults=10&q=${event.target.value}&key=${this.config.API_KEY}`, 'GET').subscribe(response => {
          console.log("response", response);
          this.results = response["items"];
          this.loading = false;
          this.localStorageService.setData("KEY_YOUTUBE_RESULTS", this.results);
        }, error => { })
      }
    }
  }

  reverseAnimation() {
    this.searchbar = "slidedown";
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.reverseAnimation();
  }

  openModal(item) {
    this.modalRef = this.modalService.open(DraggableModalComponent, this.modalOptions);
    this.modalRef.componentInstance.title = 'Drag Me!';
    this.modalRef.componentInstance.url = "https://www.youtube.com/embed/" + item.id.videoId;
  }
}
