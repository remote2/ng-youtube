import { AppConfig } from '../config/app.config';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient, private config: AppConfig) { }
  call(data, api, method) {
    const headers = new HttpHeaders();
    return this.http.request(method, this.config.api_url + api, {
      body: data,
      headers: headers
    })
  }
}
